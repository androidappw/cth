package com.example.thanhhai.helloapp;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 2/12/2018.
 */

class Image{
    int idDraw;
    ImageView imageView;

    public Image(int idDraw , Context context)
    {
        this.idDraw = idDraw;
        this.imageView = new ImageView(context);
        this.imageView.setImageResource(idDraw);
    }

    public Image(Context context) {
        Random a = new Random();
        this.idDraw = a.nextInt(1000000);
        this.imageView = new ImageView(context);
    }


}

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<Image> mImageUrls = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapter(Context context, ArrayList<Image> imageUrls) {
        mImageUrls = imageUrls;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
//        holder.image = mImageUrls.get(position).imageView;
//        mImageUrls.get(position).imageView.setImageResource(mImageUrls.get(position).idDraw);

//        holder.image.setImageMatrix(mImageUrls.get(position).imageView.getImageMatrix());
        holder.image.setImageDrawable(mImageUrls.get(position).imageView.getDrawable());

//        holder.image.setImageResource(mImageUrls.get(position).idDraw);
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.cellUser);
        }
    }
}