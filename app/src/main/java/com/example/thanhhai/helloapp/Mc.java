package com.example.thanhhai.helloapp;

public class Mc {
    String id;
    UserBasicInfo mcBasicInfo;

    public Mc() {
        id = "";
    }

    public Mc(String id, UserBasicInfo mcBasicInfo) {
        this.id = id;
        this.mcBasicInfo = mcBasicInfo;
    }
}
